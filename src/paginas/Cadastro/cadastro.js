import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import firebase from '../../Firebase';


class Cadastro extends Component{
    constructor(props){
        super(props);
        this.state = {
            email:"",
            senha:"",
            nome: "",
            sobrenome: "",
            dados:[]
        }

        this.criarUsuario = this.criarUsuario.bind(this);
        this.listar = this.listar.bind(this);
    }

    /*constructor(props){
        super(props);
        this.state = {
            nome:"",
            sobrenome:"",
            dados:[]
        }

        this.gravar = this.gravar.bind(this);
        this.listar = this.listar.bind(this);
    }

    async gravar(){
        await firebase.firestore().collection("usuario").add({
            nome: this.state.nome,
            sobrenome: this.state.sobrenome
        });
    }*/

    async criarUsuario(){
        await firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.senha)      
        .then(async (retorno) => {
            await firebase.firestore().collection("usuario").doc(retorno.user.uid).set({
                nome: this.state.nome,
                sobrenome: this.state.sobrenome
            });
        });
    }

    listar(){
        firebase.firestore().collection("usuario").get().then((retorno) => {
          const dados = [];
          retorno.forEach((item) => {
        
            dados.push({
              id: item.id,
              email: item.data().email,
              senha: item.data().senha
            });
    
          });
          this.setState({dados});
        });
    }
    

    render(){
        return(
            /*<div>
                <h1>Página de Cadastro</h1>
                <input type="text" placeholder='Nome' onChange={(e) => this.setState({nome: e.target.value})} /><br/>
                <input type="text" placeholder='Sobrenome' onChange={(e) => this.setState({sobrenome: e.target.value})} /><br/>
                <button onClick={this.gravar}>Gravar</button><br/>
                <Link to ="/"><button>Voltar</button></Link><br/><br/><br/>

                <button onClick={this.listar}>Listar</button> <br/>

                <ul>
                    {this.state.dados.map((item)=>{
                    return(
                        <li> {item.nome + " " + item.sobrenome} </li> 
                    )
                    })}
                </ul>
            </div>*/
            
            <div><br/>
                <Link to ="/"><button>Voltar</button></Link><br/>
                <h1>Cadastro</h1>
                <input type="text" placeholder='Email' onChange={(e) => this.setState({email: e.target.value})} /><br/>
                <input type="password" placeholder='Senha' onChange={(e) => this.setState({senha: e.target.value})} /><br/>
                <input type="text" placeholder='Nome' onChange={(e) => this.setState({nome: e.target.value})} /><br/>
                <input type="text" placeholder='Sobrenome' onChange={(e) => this.setState({sobrenome: e.target.value})} /><br/><br/>
                <button onClick={this.criarUsuario}>Gravar</button><br/>
                

                
            </div>

            /*<button onClick={this.listar}>Listar</button> <br/>

                <ul>
                    {this.state.dados.map((item)=>{
                    return(
                        <li>
                            {item.email + " " + "-" + " " + item.senha}
                        </li> 
                    )
                    })}
                </ul>*/


        )
    }
}

export default Cadastro;