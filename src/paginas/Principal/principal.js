import React, { Component } from 'react'
import firebase from '../../Firebase';

/*1. Chama o nome e sobrenome vazios para a atividade*/
class Principal extends Component{
    constructor(props){
        super(props);
        this.state = {
            nome: "",
            sobrenome: ""
        }
    }

    /*3. Busca as infos do UID logado e joga para o render*/
    async componentDidMount(){
        await firebase.auth().onAuthStateChanged(async(usuario)=>{
            if(usuario){
                var uid = usuario.uid;
                await firebase.firestore().collection("usuario").doc(uid).get()
                .then((retorno)=>{
                    this.setState({
                        nome: retorno.data().nome,
                        sobrenome: retorno.data().sobrenome
                    });
                });
            }
        });
    }

    /*2. Apresenta em tela as infos vazias, e em seguida chama o 3° com infos completas por UID*/
    render(){
        return(
            <div>
                Nome: {this.state.nome}<br/>
                Sobrenome: {this.state.sobrenome}
            </div>
        )
    }
}

export default Principal;