import {BrowserRouter, Route, Routes} from 'react-router-dom';

import Login from './paginas/Login/login';
import Cadastro from './paginas/Cadastro/cadastro';
import Principal from './paginas/Principal/principal';

const Rotas = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route exact={true} path="/" element={<Login/>} />
                <Route exact={true} path="/Cadastro" element={<Cadastro/>} />
                <Route exact={true} path="/Principal" element={<Principal/>} />
            </Routes>
        </BrowserRouter>
    )
}

export default Rotas;


