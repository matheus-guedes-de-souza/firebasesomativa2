import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyAfzHUzLkzPt1yheRf38u8AxaAxQ2TMrr8",
    authDomain: "atividade2tdw.firebaseapp.com",
    projectId: "atividade2tdw",
    storageBucket: "atividade2tdw.appspot.com",
    messagingSenderId: "125339005125",
    appId: "1:125339005125:web:f68c6cec355dde92bec0ac",
    measurementId: "G-KW7QT4PB85"
};

if(!firebase.apps.length){
    firebase.initializeApp(firebaseConfig);
}

export default firebase;
  